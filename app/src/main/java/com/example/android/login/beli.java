package com.example.android.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class beli extends AppCompatActivity {
    TextView bawang, sayuran, bibit, buah;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beli);

        bawang = (TextView) findViewById(R.id.bawang);
        bawang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(beli.this, bawang.class);
                beli.this.startActivity(intent);
            }
        });
        sayuran = (TextView) findViewById(R.id.sayuran);
        sayuran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(beli.this, sayuran.class);
                beli.this.startActivity(intent);
            }
        });
        bibit = (TextView) findViewById(R.id.bibit);
        bibit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(beli.this, Bibit.class);
                beli.this.startActivity(intent);
            }
        });
        buah = (TextView) findViewById(R.id.buah);
        buah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(beli.this, buah.class);
                beli.this.startActivity(intent);
            }
        });
    }
}
